FROM jupyter/minimal-notebook

USER root

RUN apt-get update \
  && apt-get -y install netcat gcc postgresql build-essential libpq-dev \
  && apt-get clean

USER ${NB_UID}

COPY jupyter_notebook_config.py /home/jovyan/.jupyter/

ENV PYTHONPATH "${PYTHONPATH}:/home/jovyan/lets_learn_sql"

RUN pip install --upgrade pip
COPY requirements.txt .
RUN pip install --no-cache-dir --upgrade -r requirements.txt