notebook==6.5.1
psycopg2==2.9.4
pytest==7.2.0
python-dotenv==0.21.0
SQLAlchemy==1.4.42