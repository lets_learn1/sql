import os
from dotenv import load_dotenv
from sqlalchemy import create_engine

load_dotenv("../../.env")

postgres_host = os.getenv("POSTGRES_HOST")
postgres_port = int(os.getenv("POSTGRES_PORT"))
postgres_user = os.getenv("POSTGRES_USER")
postgres_password = os.getenv("POSTGRES_PASSWORD")
postgres_db = os.getenv("POSTGRES_DB")
postgres_connection = f"postgresql://{postgres_user}:{postgres_password}@{postgres_host}:{postgres_port}/{postgres_db}"

engine = create_engine(postgres_connection)


def run_ddl(sql_text: str):
    with engine.connect() as connection:
        connection.execute(sql_text)


def run_copy(file_path: str, table_name: str):
    connection = engine.raw_connection()
    with connection.cursor() as cursor:
        with open(file_path) as f:
            cursor.copy_from(f, table_name, sep=',', null="NULL")
            connection.commit()


def run_sql_default_user(sql_text: str):
    with engine.connect() as connection:
        result = connection.execute(sql_text).fetchall()
        if result:
            result = result.fetchall()
        return result
