# Lets Learn SQL

## Purpose
This module was created with the purpose of providing an environment that is both safe and recoverable.  Many courses exist that will highlight the fundamentals of SQL while neglecting or assuming the student has the knowledge or environment already at hand to work with the concepts that are being taught.  This course makes no such assumptions and attempts to provide an interface that allows the learner the ability for trial and error without consequence.

## How
This repository makes use of a few different tools that allow the learner to focus on learning SQL itself.  These tools are:
* [git](https://git-scm.com/downloads) - Git is a version control system that in conjunction with a host such as Github or Gitlab allow for easy versioning and distribution of software.  We will use it to clone the repository to our local machine.
* [docker](https://www.docker.com/products/docker-desktop/) - Docker is a containerization tool that allows for a predefined installation and execution environment for software.  Docker Desktop is an easy way to install its various tools.  We will use it to run our Jupyter notebooks, database, and tests.
* [jupyter](https://jupyter.org/) - Jupyter is an execution environment that allows for code execution.  This will serve as our way to set up, reset, run queries, and test against our database.

If at all possible, the goal of this course is you would only need to install the above tools and then work solely out of Jupyter notebooks and your preferred database IDE.

## Initial Set Up
First steps are to get your local machine ready for development.  To do so, follow these steps:
1. Download [git](https://git-scm.com/downloads) and install
    - some computers will already have this installed so if you'd to check, open Command Prompt/Terminal and run ```git --version```.  If git is installed, then a message like ```git version 2.37.0``` will be returned.
2. Download [Docker Desktop](https://www.docker.com/products/docker-desktop/) and install
3. Download a database IDE.  Some suggestions are below:
   - [Jetbrains DataGrip](https://www.jetbrains.com/datagrip/) - (personal favorite) while this is a paid option, if you are student with a valid student email address you can get free access to this software through the Jetbrains student program
   - [pgAdmin](https://www.pgadmin.org/) - if working with a Postgres database like this course is based off of, this is a free option
4. Open Command Prompt/Terminal and go to the directory where you would like to store the project code.  To do so, use the ```cd path``` command until you're in the location you want.
5. Run the following command to clone the repository to your local machine.  This may require you to make a Gitlab account for user details.
   ```commandLine
   git clone https://gitlab.com/lets_learn1/sql lets_learn_sql
   ```
6. Once the project has been cloned, run the following in Command Prompt/Terminal
   ```commandLine
   cd lets_learn_sql
   ```
7. Next, to start up all the software we are going to use docker-compose.  Docker-compose is a tool provided by Docker that allows for complex Docker deployments to be made.  Run the following in Command Prompt/Terminal:
   ```commandline
   docker-compose up -d
   ```
   Once completed, Postgres and Jupyter will be up and running.
8. Confirm Jupyter is online by visiting http://localhost:8888.  You should be met with a login page, use the credentials below to login.
   ```commandline
   Password: jupyter
   ```
9. Now we need to set up our database IDE.  I'm going to assume you are using DataGrip and the subsequent steps will reflect that.  If you are using a different software it will likely be similar so attempt to map the steps.
10. Start DataGrip
11. You will be required to choose a workspace, select the repository where we cloned the git repo.
12. We want to create a new connection to our Postgres database.  To do so, in the Database Explorer, click the + symbol -> Data Source -> PostgreSQL
   ![](./resources/images/connection_setup.png)
13. Enter the connection details as described below:
   ![](./resources/images/connection_details.png)
   ```commandline
   Host: localhost
   Port: 5432
   User: postgres
   Password: postgres
   Database: postgres
   ```
   NOTE: As this is something that is running locally and is never to be exposed to the outside world, I've included default connection details here.  However, I feel it is important to highlight how much of a BAD idea this is.  NEVER include passwords or any other secrets with your repositories in regular scenarios.  This was done in this case solely to help facilitate learning in an isolated environment.  
14. After the connection details have been entered, if a message saying "Download missing driver files" is present click Download.  This will automatically install everything needed to make a connection to the database.
15. Once all the drivers are present, click Test Connection.  If everything worked correctly, you will get a message saying it was successful.  If successful, click OK.  You should see the connection "localhost" with database "postgres".
   ![](./resources/images/connection_added.png)
16. That's all!  You now have a working database and code execution environment!

## Next Steps
So we have a database and an area where we can write code, what comes next?  The normal tempo each lesson will follow will be to create the necessary database objects, populate those objects with the data each lesson needs, and then walk through the topic covered by that lesson.  This will all be done in Jupyter with an individual notebook for each lesson.  These can be found at [lessons](./lessons) with each lesson containing a lesson.ipynb file that will act as your guide.

All you will need to do is get accustomed to how to operate a Jupyter notebook.  To do so, visit the [example lesson](http://localhost:8888/lab/tree/lets_learn_sql/lessons/example_lesson/lesson.ipynb) in Jupyter which will run through what to expect when working through a lesson.